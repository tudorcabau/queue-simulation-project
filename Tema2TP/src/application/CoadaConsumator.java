package application;
import java.util.Date;
import java.util.concurrent.BlockingQueue;

public class CoadaConsumator implements Runnable{
private BlockingQueue<Client> coada;
private Coada coad ;
boolean coadaGoala = false;
private String log = new String();
private static long timpulPetrecutLaCoada;
private static long totalEmptyTime;
private String idCoadaConsumator;
public CoadaConsumator(Coada q){
	this.coad = q;
	this.coada = coad.getArray();

}

@Override
public void run() {	
	try {
		Client client;
		while((client = coada.take()).getId() !="Exit"){
			coada.add(client);
			if(this.coadaGoala){
				Date stopEmpty = new Date();
				coad.setStopEmty(stopEmpty.getTime());
				coad.emptyTimeRepriza();
				Cozi.setTotalEmptyTime(coad.getEmptyTimeRepriza());
				System.out.println("coada a fost goala timp de : " + coad.getEmptyTimeRepriza());
				Cozi.setLog("Coada a fost goala timp de : " + coad.getEmptyTimeRepriza());				
				coadaGoala = false;}		
			Thread.sleep(client.getTimpNecesar());
			Date momentulIesiri = new Date();
			client.setMomentulIesiri(momentulIesiri.getTime());
			client.setTimpulAsteptat();
			Cozi.setLog("Scos client  "+client.getId()+" de la coada " + coad.getId() + " timpul petrecut la coada "+client.getTimpulAsteptat()/1000+" s");
			System.out.println("Scos client  "+client.getId()+" la coada " + coad.getId() + " timpul petrecut la coada "+client.getTimpulAsteptat()/1000+" s" + "tim coada goala total: " + coad.getTotalWaintingTime());
			timpulPetrecutLaCoada += client.getTimpulAsteptat();
			client = coada.take();
			if (coada.isEmpty()){
				this.coadaGoala = true;
				Date startEmpty = new Date();
			coad.setStartEmpty(startEmpty.getTime());
			}	
		}
	}catch (InterruptedException e) {
		e.printStackTrace();
	}
}



	public void setIdCoadaConsumator(String id){
		this.idCoadaConsumator = id;
	}
	public void addElementInCoada(Client client){
		coada.add(client);
		
	}
	public BlockingQueue<Client> getCoada(){
		return this.coada;
	}
	public static long getTimpulTotalAteptare(){
		return timpulPetrecutLaCoada;
		
	}

	
}
