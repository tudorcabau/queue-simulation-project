package application;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;



	
	public class Main extends Application{
		@Override
		public void start(Stage primaryStage) throws Exception{
		Parent root = FXMLLoader.load(getClass().getResource("GUITema2.fxml"));
		primaryStage.setTitle("AplicatieCozi");
		primaryStage.setScene(new Scene(root));
		primaryStage.show();		
		}
	
	public static void main(String[] args) {
		System.out.println("Threadurile au fost pornite");
		launch(args);
	}

}
