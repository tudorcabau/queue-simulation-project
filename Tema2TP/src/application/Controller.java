package application;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.control.Label;

public class Controller implements Initializable {



	   

	    @FXML
	    private Text logField;

	    @FXML
	    private Label l1;

	    @FXML
	    private Label l2;

	    @FXML
	    private Label l3;

	    @FXML
	    private Label l4;

	    @FXML
	    private Label l5;

	    @FXML
	    private Label l6;

	    @FXML
	    private Label l7;

	    @FXML
	    private Label l8;

	    @FXML
	    private Label l9;

	    @FXML
	    private Label l10;

	    @FXML
	    private Label l11;

	    @FXML
	    private Button butonStart;

	    @FXML
	    private Button butonStop;

	    @FXML
	    private Label l21;

	    @FXML
	    private Label l22;

	    @FXML
	    private Label l23;

	    @FXML
	    private Label l24;

	    @FXML
	    private Label l25;

	    @FXML
	    private Label l26;

	    @FXML
	    private Label l27;

	    @FXML
	    private Label l28;

	    @FXML
	    private Label l29;

	    @FXML
	    private Label l210;

	    @FXML
	    private Label l211;

	    @FXML
	    private Label l31;

	    @FXML
	    private Label l32;

	    @FXML
	    private Label l33;

	    @FXML
	    private Label l34;

	    @FXML
	    private Label l35;

	    @FXML
	    private Label l36;

	    @FXML
	    private Label l37;

	    @FXML
	    private Label l38;

	    @FXML
	    private Label l39;

	    @FXML
	    private Label l310;

	    @FXML
	    private Label l311;

	    @FXML
	    private Label l41;

	    @FXML
	    private Label l42;

	    @FXML
	    private Label l43;

	    @FXML
	    private Label l44;

	    @FXML
	    private Label l45;

	    @FXML
	    private Label l46;

	    @FXML
	    private Label l47;

	    @FXML
	    private Label l48;

	    @FXML
	    private Label l49;

	    @FXML
	    private Label l410;

	    @FXML
	    private Label l411;

	    @FXML
	    private TextField textMinAstept;

	    @FXML
	    private TextField textMaxAstept;

	    @FXML
	    private TextField textMinSosire;

	    @FXML
	    private TextField textMaxSosire;

	    @FXML
	    private TextField durataExecutie;

	    @FXML
	    private TextField nrCoziText; 
	   

    Cozi cozi ;
    Thread threadCoada1,threadCoada2,threadCoada3,threadCoada4,threadLogger,threadPeekHour;
    
    Thread threadProducator;
    Thread threadCozi;
    CoadaProducator producator;
    int nrCozi;
    //CoadaConsumator consumator;
    Thread threadConsumator,threadshowPeekHour;
    
    Task coada1 = new Task<Void>(){
    	@Override
    	  public Void call() throws Exception {
    	    while (true) {
    	      Platform.runLater(new Runnable() {
    	        @Override
    	        public void run() {

    	        	if(nrCozi>0){
    	      	int nrClientiPrimaCoada = cozi.getLista().get(0).nrClienti();

    			switch(nrClientiPrimaCoada){
    			case(0):
    			l1.setVisible(false);
    			l2.setVisible(false);
    			l3.setVisible(false);
    			l4.setVisible(false);
    			l5.setVisible(false);
    			l6.setVisible(false);
    			l7.setVisible(false);
    			l8.setVisible(false);
    			l9.setVisible(false);
    			l10.setVisible(false);
    			l11.setVisible(false);
    			break;
    			case(1):
    			l1.setVisible(true);
    			l2.setVisible(false);
    			l3.setVisible(false);
    			l4.setVisible(false);
    			l5.setVisible(false);
    			l6.setVisible(false);
    			l7.setVisible(false);
    			l8.setVisible(false);
    			l9.setVisible(false);
    			l10.setVisible(false);
    			l11.setVisible(false);
    			break;
    			case(2):
    			l1.setVisible(true);
    			l2.setVisible(true);
    			l3.setVisible(false);
    			l4.setVisible(false);
    			l5.setVisible(false);
    			l6.setVisible(false);
    			l7.setVisible(false);
    			l8.setVisible(false);
    			l9.setVisible(false);
    			l10.setVisible(false);
    			l11.setVisible(false);
    			break;
    			case(3):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(false);
        			l5.setVisible(false);
        			l6.setVisible(false);
        			l7.setVisible(false);
        			l8.setVisible(false);
        			l9.setVisible(false);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(4):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(false);
        			l6.setVisible(false);
        			l7.setVisible(false);
        			l8.setVisible(false);
        			l9.setVisible(false);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(5):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(true);
        			l6.setVisible(false);
        			l7.setVisible(false);
        			l8.setVisible(false);
        			l9.setVisible(false);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(6):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(true);
        			l6.setVisible(true);
        			l7.setVisible(false);
        			l8.setVisible(false);
        			l9.setVisible(false);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(7):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(true);
        			l6.setVisible(true);
        			l7.setVisible(true);
        			l8.setVisible(false);
        			l9.setVisible(false);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(8):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(true);
        			l6.setVisible(true);
        			l7.setVisible(true);
        			l8.setVisible(true);
        			l9.setVisible(false);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(9):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(true);
        			l6.setVisible(true);
        			l7.setVisible(true);
        			l8.setVisible(true);
        			l9.setVisible(true);
        			l10.setVisible(false);
        			l11.setVisible(false);
        			break;
    			case(10):
        			l1.setVisible(true);
        			l2.setVisible(true);
        			l3.setVisible(true);
        			l4.setVisible(true);
        			l5.setVisible(true);
        			l6.setVisible(true);
        			l7.setVisible(true);
        			l8.setVisible(true);
        			l9.setVisible(true);
        			l10.setVisible(true);
        			l11.setVisible(false);
        			break;
    		
    			default:
    				l1.setVisible(false);
    				l2.setVisible(false);
    				l3.setVisible(false);
    				l4.setVisible(false);
    				l5.setVisible(false);
    				l6.setVisible(false);
    				l7.setVisible(false);
    				l8.setVisible(false);
    				l9.setVisible(false);
    				l10.setVisible(false);
    				l11.setVisible(false);
    			}  	          
    	        }else{
    	        	
        	        	l31.setVisible(false);
                        l32.setVisible(false);
                        l33.setVisible(false);
                        l34.setVisible(false);
                        l35.setVisible(false);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
    	        }
    	        }
    	      });
    	      Thread.sleep(50);
    	    }
    	  }  	
    };
    Task coada2 = new Task<Void>(){
    	@Override
    	  public Void call() throws Exception {
    	    while (true) {
    	      Platform.runLater(new Runnable() {
    	        @Override
    	        public void run() {

    	        	if(nrCozi>1){
    	      	int nrClientiPrimaCoada = cozi.getLista().get(1).nrClienti();

    			switch(nrClientiPrimaCoada){
    			case(0):
                    l21.setVisible(false);
                    l22.setVisible(false);
                    l23.setVisible(false);
                    l24.setVisible(false);
                    l25.setVisible(false);
                    l26.setVisible(false);
                    l27.setVisible(false);
                    l28.setVisible(false);
                    l29.setVisible(false);
                    l210.setVisible(false);
                    l211.setVisible(false);
                    break;
                    case(1):
                    l21.setVisible(true);
                    l22.setVisible(false);
                    l23.setVisible(false);
                    l24.setVisible(false);
                    l25.setVisible(false);
                    l26.setVisible(false);
                    l27.setVisible(false);
                    l28.setVisible(false);
                    l29.setVisible(false);
                    l210.setVisible(false);
                    l211.setVisible(false);
                    break;
                    case(2):
                    l21.setVisible(true);
                    l22.setVisible(true);
                    l23.setVisible(false);
                    l24.setVisible(false);
                    l25.setVisible(false);
                    l26.setVisible(false);
                    l27.setVisible(false);
                    l28.setVisible(false);
                    l29.setVisible(false);
                    l210.setVisible(false);
                    l211.setVisible(false);
                    break;
                    case(3):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(false);
                        l25.setVisible(false);
                        l26.setVisible(false);
                        l27.setVisible(false);
                        l28.setVisible(false);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
                        break;
                    case(4):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(false);
                        l26.setVisible(false);
                        l27.setVisible(false);
                        l28.setVisible(false);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
                      break;
                    case(5):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(true);
                        l26.setVisible(false);
                        l27.setVisible(false);
                        l28.setVisible(false);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
                        break;
                    case(6):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(true);
                        l26.setVisible(true);
                        l27.setVisible(false);
                        l28.setVisible(false);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
                        break;
                    case(7):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(true);
                        l26.setVisible(true);
                        l27.setVisible(true);
                        l28.setVisible(false);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
                        break;
                    case(8):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(true);
                        l26.setVisible(true);
                        l27.setVisible(true);
                        l28.setVisible(true);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
                        break;
                    case(9):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(true);
                        l26.setVisible(true);
                        l27.setVisible(true);
                        l28.setVisible(true);
                        l29.setVisible(true);
                        l210.setVisible(false);
                        l211.setVisible(false);
                        break;
                    case(10):
                        l21.setVisible(true);
                        l22.setVisible(true);
                        l23.setVisible(true);
                        l24.setVisible(true);
                        l25.setVisible(true);
                        l26.setVisible(true);
                        l27.setVisible(true);
                        l28.setVisible(true);
                        l29.setVisible(true);
                        l210.setVisible(true);
                        l211.setVisible(false);
                        break;
               
                    default:
                        l21.setVisible(false);
                        l22.setVisible(false);
                        l23.setVisible(false);
                        l24.setVisible(false);
                        l25.setVisible(false);
                        l26.setVisible(false);
                        l27.setVisible(false);
                        l28.setVisible(false);
                        l29.setVisible(false);
                        l210.setVisible(false);
                        l211.setVisible(false);
    			}  	          
    	        }else{
    	        
        	        	l31.setVisible(false);
                        l32.setVisible(false);
                        l33.setVisible(false);
                        l34.setVisible(false);
                        l35.setVisible(false);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
    	        }
    	        }
    	      });
    	      Thread.sleep(50);
    	    }
    	  }  	
    };
    Task coada3 = new Task<Void>(){
    	@Override
    	  public Void call() throws Exception {
    	    while (true) {
    	      Platform.runLater(new Runnable() {
    	        @Override
    	        public void run() {

    	        	if(nrCozi>2){
    	      	int nrClientiPrimaCoada = cozi.getLista().get(2).nrClienti();

    			switch(nrClientiPrimaCoada){
    			case(0):
                    l31.setVisible(false);
                    l32.setVisible(false);
                    l33.setVisible(false);
                    l34.setVisible(false);
                    l35.setVisible(false);
                    l36.setVisible(false);
                    l37.setVisible(false);
                    l38.setVisible(false);
                    l39.setVisible(false);
                    l310.setVisible(false);
                    l311.setVisible(false);
                    break;
                    case(1):
                    l31.setVisible(true);
                    l32.setVisible(false);
                    l33.setVisible(false);
                    l34.setVisible(false);
                    l35.setVisible(false);
                    l36.setVisible(false);
                    l37.setVisible(false);
                    l38.setVisible(false);
                    l39.setVisible(false);
                    l310.setVisible(false);
                    l311.setVisible(false);
                    break;
                    case(2):
                    l31.setVisible(true);
                    l32.setVisible(true);
                    l33.setVisible(false);
                    l34.setVisible(false);
                    l35.setVisible(false);
                    l36.setVisible(false);
                    l37.setVisible(false);
                    l38.setVisible(false);
                    l39.setVisible(false);
                    l310.setVisible(false);
                    l311.setVisible(false);
                    break;
                    case(3):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(false);
                        l35.setVisible(false);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(4):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(false);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(5):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(true);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(6):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(true);
                        l36.setVisible(true);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(7):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(true);
                        l36.setVisible(true);
                        l37.setVisible(true);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(8):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(true);
                        l36.setVisible(true);
                        l37.setVisible(true);
                        l38.setVisible(true);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(9):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(true);
                        l36.setVisible(true);
                        l37.setVisible(true);
                        l38.setVisible(true);
                        l39.setVisible(true);
                        l310.setVisible(false);
                        l311.setVisible(false);
                        break;
                    case(10):
                        l31.setVisible(true);
                        l32.setVisible(true);
                        l33.setVisible(true);
                        l34.setVisible(true);
                        l35.setVisible(true);
                        l36.setVisible(true);
                        l37.setVisible(true);
                        l38.setVisible(true);
                        l39.setVisible(true);
                        l310.setVisible(true);
                        l311.setVisible(false);
                        break;
             
                    default:
                        l31.setVisible(false);
                        l32.setVisible(false);
                        l33.setVisible(false);
                        l34.setVisible(false);
                        l35.setVisible(false);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
    			}  	          
    	        }else{
    	        	
        	        	l31.setVisible(false);
                        l32.setVisible(false);
                        l33.setVisible(false);
                        l34.setVisible(false);
                        l35.setVisible(false);
                        l36.setVisible(false);
                        l37.setVisible(false);
                        l38.setVisible(false);
                        l39.setVisible(false);
                        l310.setVisible(false);
                        l311.setVisible(false);
    	        }
    	        }
    	      });
    	      Thread.sleep(50);
    	    }
    	  }  	
    };
    Task coada4 = new Task<Void>(){
    	@Override
    	  public Void call() throws Exception {
    	    while (true) {
    	      Platform.runLater(new Runnable() {
    	        @Override
    	        public void run() {
    	        if(nrCozi>3){
    	      	int nrClientiPrimaCoada = cozi.getLista().get(3).nrClienti();

    			switch(nrClientiPrimaCoada){
    			case(0):
                    l41.setVisible(false);
                    l42.setVisible(false);
                    l43.setVisible(false);
                    l44.setVisible(false);
                    l45.setVisible(false);
                    l46.setVisible(false);
                    l47.setVisible(false);
                    l48.setVisible(false);
                    l49.setVisible(false);
                    l410.setVisible(false);
                    l411.setVisible(false);
                    break;
                    case(1):
                    l41.setVisible(true);
                    l42.setVisible(false);
                    l43.setVisible(false);
                    l44.setVisible(false);
                    l45.setVisible(false);
                    l46.setVisible(false);
                    l47.setVisible(false);
                    l48.setVisible(false);
                    l49.setVisible(false);
                    l410.setVisible(false);
                    l411.setVisible(false);
                    break;
                    case(2):
                    l41.setVisible(true);
                    l42.setVisible(true);
                    l43.setVisible(false);
                    l44.setVisible(false);
                    l45.setVisible(false);
                    l46.setVisible(false);
                    l47.setVisible(false);
                    l48.setVisible(false);
                    l49.setVisible(false);
                    l410.setVisible(false);
                    l411.setVisible(false);
                    break;
                    case(3):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(false);
                        l45.setVisible(false);
                        l46.setVisible(false);
                        l47.setVisible(false);
                        l48.setVisible(false);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(4):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(false);
                        l46.setVisible(false);
                        l47.setVisible(false);
                        l48.setVisible(false);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(5):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(true);
                        l46.setVisible(false);
                        l47.setVisible(false);
                        l48.setVisible(false);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(6):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(true);
                        l46.setVisible(true);
                        l47.setVisible(false);
                        l48.setVisible(false);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(7):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(true);
                        l46.setVisible(true);
                        l47.setVisible(true);
                        l48.setVisible(false);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(8):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(true);
                        l46.setVisible(true);
                        l47.setVisible(true);
                        l48.setVisible(true);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(9):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(true);
                        l46.setVisible(true);
                        l47.setVisible(true);
                        l48.setVisible(true);
                        l49.setVisible(true);
                        l410.setVisible(false);
                        l411.setVisible(false);
                        break;
                    case(10):
                        l41.setVisible(true);
                        l42.setVisible(true);
                        l43.setVisible(true);
                        l44.setVisible(true);
                        l45.setVisible(true);
                        l46.setVisible(true);
                        l47.setVisible(true);
                        l48.setVisible(true);
                        l49.setVisible(true);
                        l410.setVisible(true);
                        l411.setVisible(false);
                        break;
            
                    default:
                        l41.setVisible(false);
                        l42.setVisible(false);
                        l43.setVisible(false);
                        l44.setVisible(false);
                        l45.setVisible(false);
                        l46.setVisible(false);
                        l47.setVisible(false);
                        l48.setVisible(false);
                        l49.setVisible(false);
                        l410.setVisible(false);
                        l411.setVisible(false);
    			}
    			}else{
    				l41.setVisible(false);
                    l42.setVisible(false);
                    l43.setVisible(false);
                    l44.setVisible(false);
                    l45.setVisible(false);
                    l46.setVisible(false);
                    l47.setVisible(false);
                    l48.setVisible(false);
                    l49.setVisible(false);
                    l410.setVisible(false);
                    l411.setVisible(false);
    				
    			}  	          
    	        }
    	      });
    	      Thread.sleep(50);
    	    }
    	  }  	
    };
    
    int nrTotalClienti = 0;
    int buffTotalClienti = 0;
    String logCozi;
    Date d;
    int asa=0;  
    Task peekHour = new Task<Void>(){
    @Override
	  public Void call() throws Exception {
	    while (true) {
	      Platform.runLater(new Runnable() {
	        @Override
	        public void run() {	        
	        for(Coada q : cozi.getCozi()){
	        	buffTotalClienti += q.getSize();
	        	
	        	}
	        if(buffTotalClienti>nrTotalClienti){
	        	nrTotalClienti = buffTotalClienti;
	        	d = new Date();
	        	Cozi.setPeek("Peek Hour " + d.getHours() + ":" + d.getMinutes());
	        }
	        buffTotalClienti = 0;
	        }
	        });
	      Thread.sleep(100); 
	      }	    
    }
    };
    
    Task logger = new Task<Void>(){
    @Override
	  public Void call() throws Exception {
	    while (true) {
	      Platform.runLater(new Runnable() {
	        @Override
	        public void run() {
	        	logCozi = new String();
	        	logCozi = cozi.getLog().toString();
	        	logField.setText(logCozi);     	
	        }
	        
	        });
	      Thread.sleep(10);
    
	      }
	    
    }
    };
    
    Task showPeekHour = new Task<Void>(){
        @Override
    	  public Void call() throws Exception {
    	    while (true) {
    	      Platform.runLater(new Runnable() {
    	        @Override
    	        public void run() {
    	        	if(!threadCozi.isAlive()){
    	        	String temp = new String (System.lineSeparator() + System.lineSeparator() +"Peek Hour"+ d.getHours()+ ":" + d.getMinutes() + ":"+ d.getSeconds());
    	        	
    		         		cozi.getLog().append(temp);
    		         		temp = System.lineSeparator() + " Averrage Wainting Time= " + Cozi.getAverageWaitingTime() + "ms";
    		         		cozi.getLog().append(temp);
    		         		temp = System.lineSeparator() + " Average Service Time= " + Cozi.getAverageServiceTime() + "ms "; 
    		         		cozi.getLog().append(temp);
    		         		temp = System.lineSeparator() + " Total Empty Queue Time= " + Cozi.getTotalEmptyTime() + "ms "; 
    		         		cozi.getLog().append(temp);
    		         		logField.setText(logCozi);
    		         		threadshowPeekHour.interrupt();
    		         		return;
    		        	 }
    	        }
    	        });
    	      Thread.sleep(Long.parseLong(textMaxAstept.getText())+1);
        
    	      }
    	    
        }
        };
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
		butonStart.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent clickButon) {
			nrCozi=Integer.parseInt(nrCoziText.getText());
			
			 cozi = new Cozi(nrCozi);
			 Cozi.resetLog();
			 
			 threadCoada1 = new Thread(coada1);
			 threadCoada2 = new Thread(coada2);
			 threadCoada3 = new Thread(coada3);
			 threadCoada4 = new Thread(coada4);
			 threadLogger = new Thread(logger);
			 threadPeekHour = new Thread(peekHour);
			 threadshowPeekHour = new Thread(showPeekHour);
			 threadCoada1.start();
			 threadCoada2.start();
			 threadCoada3.start();
			 threadCoada4.start();
			 threadLogger.start();
			 threadPeekHour.start();
			 threadshowPeekHour.start();
			 
			 producator= new CoadaProducator(cozi.coadaTemporara(),Long.parseLong(durataExecutie.getText())*60000);
			 producator.setMinTimpClientNou(Integer.parseInt(textMinSosire.getText()));
			 producator.setMaxTimpClientNou(Integer.parseInt(textMaxSosire.getText()));
			 producator.setMinTimpNecesar(Integer.parseInt(textMinAstept.getText()));
			 producator.setMaxTimpNecesar(Integer.parseInt(textMaxAstept.getText()));
			 threadProducator = new Thread(producator);
			 threadCozi = new Thread(cozi);	
			 threadProducator.start();
			 threadCozi.start();
			}
			});
		butonStop.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent butnoStop) {
			Runtime rt = Runtime.getRuntime();
			try {
				Process process = rt.exec("C:\\Program Files\\Notepad++\\notepad++.exe C:\\Users\\cabau\\workspace\\BLah\\logger.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			threadCoada1.interrupt();
			 threadCoada2.interrupt();
			 threadCoada3.interrupt();
			 threadCoada4.interrupt();
			 threadLogger.interrupt();
			 threadPeekHour.interrupt();
			 threadshowPeekHour.interrupt();
			threadPeekHour.stop();
			threadProducator.stop();
			threadCozi.stop();
			
			}
			});	
    }

}

