package application;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.lang.Math;
public class CoadaProducator implements Runnable{
	private BlockingQueue<Client> coada ;
	private int minTimpNecesar,maxTimpNecesar,minTimpClientNou,maxTimpClientNou;
	private static long timpTotalServire;
	private static int totalClientiProdusi;
	private long timpStop;
	private String log = new String();
	private long durataRulare,momentPrezent,momentTerminare;
	private Random rn = new Random();
	Date d = new Date();
	private int i=0;
	public CoadaProducator(BlockingQueue<Client> q,long durataRulare){
		this.durataRulare = durataRulare;
		System.out.println("durata rulare" + durataRulare);
		momentTerminare = d.getTime() + durataRulare;
		this.coada = q;
	}
	
	@Override
	public void run() {
		
		while(momentPrezent <= momentTerminare){
			Client client = new Client(""+i);
			i++;
			totalClientiProdusi = totalClientiProdusi +1 ;
			int timpRandomNecesar = rn.nextInt(maxTimpNecesar-minTimpNecesar)+minTimpNecesar;
			timpTotalServire = timpTotalServire + timpRandomNecesar;
			int timpRandomSosireClientNou = rn.nextInt(maxTimpClientNou-minTimpClientNou)+minTimpClientNou;			
			client.setTimpNecesar(timpRandomNecesar);	
			try{
				Thread.sleep(timpRandomSosireClientNou);
				Date momentulAdaugariiInLista = new Date();
				client.setMomentulIntrari(momentulAdaugariiInLista.getTime());
				coada.add(client);
				Cozi.setLog("Generat client numarul: "+client.getId() + " timpul necesar servirii "+ client.getTimpNecesar()/1000 );
				System.out.println("Generat client numarul: "+client.getId() + " timpul necesar servirii "+ client.getTimpNecesar()/1000 );
				}
			catch(InterruptedException e){
				e.printStackTrace();
			}
			Date momentCurent = new Date();
			momentPrezent = momentCurent.getTime();
		}
		Client client = new Client ("Exit");
		coada.add(client);
	}
	public int getMinTimpNecesar() {
		return minTimpNecesar;
	}

	public void setMinTimpNecesar(int minTimpNecesar) {
		if(minTimpNecesar !=0)
		this.minTimpNecesar = minTimpNecesar;
		else{this.minTimpNecesar = 1000;}
	}

	public int getMaxTimpNecesar() {
		return maxTimpNecesar;
	}

	public void setMaxTimpNecesar(int maxTimpNecesar) {
		if(maxTimpNecesar !=0)
		this.maxTimpNecesar = maxTimpNecesar;
		else{this.maxTimpNecesar = 1500;}
	}

	public int getMinTimpClientNou() {
		
		return minTimpClientNou;
	}

	public void setMinTimpClientNou(int minTimpClientNou) {
		if(minTimpClientNou !=0)
		this.minTimpClientNou = minTimpClientNou;
		else{this.minTimpClientNou = 800;}
	}

	public int getMaxTimpClientNou() {
		return maxTimpClientNou;
	}

	public void setMaxTimpClientNou(int maxTimpClientNou) 
	{	if(maxTimpClientNou !=0)
		this.maxTimpClientNou = maxTimpClientNou;
	else{this.maxTimpClientNou = 1500;}
	}
	public long getDurataRulare() {
		return durataRulare;
	}
	public void setDurataRulare(long durataRulare) {
		this.durataRulare = durataRulare;
	}
	public static int getTotalClientiProdusi(){
		return totalClientiProdusi;
	}
	public static long getTotalSeviceTime(){
		return timpTotalServire;
		
	}
}
