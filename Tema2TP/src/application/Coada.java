package application;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Coada {
	
private BlockingQueue<Client> coada;
//private int curentQueueWaitingTime;
private String idCoada;
private int curentQueueWaitingTime = 0,totalClientiProcesati;
private long totalWaitinTime,emptyTime,startEmpty,stopEmpty;
private boolean isEmpty = false;
private long inceputEmpty,sfarsitEmpty;
private long reprizaEmtpy;
private int clientiServiti = 0;


public Coada(String id){
	this.idCoada = id;
	this.coada =  new ArrayBlockingQueue<>(10);
	
}

	public int getSize(){
		return this.coada.size();	
	}
	public void addClient(Client client){
		coada.add(client);
	}
	public int getWaitingTime(){
		
	for(Client cl : coada){
		curentQueueWaitingTime +=1;	
	}
	return curentQueueWaitingTime;
	}
	public int nrClienti(){
		int nr = coada.size();
		return nr;
	}
	public BlockingQueue<Client> getArray(){
		return this.coada;
		
	}
	public void setTotalWaitingTime(long timp){
		totalWaitinTime = totalWaitinTime + timp;
	}
	public void setNumarTotalClientiProcesati(){
		totalClientiProcesati = totalClientiProcesati + 1;
	}
	
	public void startEmtpy(){
		Date d = new Date();
		this.inceputEmpty = d.getTime();
		this.isEmpty = true;
	}
	public void setStartEmpty(long setStartEmtpy){
		this.startEmpty = setStartEmtpy;
	}
	public void setStopEmty(long setStopEmpty){
		this.stopEmpty = setStopEmpty;
	}
	public void emptyTimeRepriza(){
		this.reprizaEmtpy = stopEmpty - startEmpty;
		totalWaitinTime = totalWaitinTime + reprizaEmtpy;
	}
	public long getEmptyTimeRepriza(){
		return this.reprizaEmtpy;
		
	}
	public long getTotalWaintingTime(){
		return this.totalWaitinTime;
	}
	public String getId(){
		return this.idCoada;
	}
	public void totalClientiServiti(){
		clientiServiti = clientiServiti + 1; 
	}
	
}
