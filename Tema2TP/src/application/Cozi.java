package application;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.spi.TimeZoneNameProvider;

import javafx.scene.text.Text;

public class Cozi implements Runnable{
private ArrayList<Coada> listaCozi = new ArrayList<>(4);
private int curentQueueWaitingTime,id;
private BlockingQueue<Client> c1 = new ArrayBlockingQueue<>(10) ;
private int totalClienti=0;
private static StringBuilder log = new StringBuilder();
private static PrintStream out;
private Thread threaduriCozi[];
int nrCozi;
private static long timpTotalPentrecutLaCoada,totalEmptyTimeToateCozile;
private static String peekHour = new String();
private static boolean toateCozileSuntGoale = false;
public Cozi(){}
public Cozi(int nrCozi){
	this.nrCozi=nrCozi;
 	try {
 		 out = new PrintStream("logger.txt");
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
 	threaduriCozi = new Thread[nrCozi];
	for(int i = 0;i<nrCozi;i++){
	Coada c = new Coada("Coada-Fabricata"+i);
	listaCozi.add(c);
	CoadaConsumator consumator = new CoadaConsumator(c);	
	threaduriCozi[i] = new Thread(consumator);
	threaduriCozi[i].start();
	}
}

@Override
public void run() {
	try {
		Client client;
		while((client = c1.take()).getId() !="Exit"){	
			synchronized (listaCozi) {
			totalClienti++;		
			int indexCeaMaiMicaCoada = ceaMaiMicaCoada();
			listaCozi.get(indexCeaMaiMicaCoada).setTotalWaitingTime(client.getTimpulAsteptat());
			listaCozi.get(indexCeaMaiMicaCoada).addClient(client);
			listaCozi.get(indexCeaMaiMicaCoada).setNumarTotalClientiProcesati();
			setLog("Client "+client.getId()+" adaugat la index coada " + indexCeaMaiMicaCoada);
			System.out.println("Client "+client.getId()+" adaugat la index coada " + indexCeaMaiMicaCoada);			
			}			
		}	
		while(toateCozileSuntGoale == false){
			int temporar= 0;
			for(int i=0;i<nrCozi;i++){
				if(listaCozi.get(i).getSize()==0){
					temporar = temporar + 1;	
				}		
			}
			if(temporar == nrCozi){
				toateCozileSuntGoale = true;
			}else{toateCozileSuntGoale = false;}
			}
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
}


public int coadaCuCelMaiMicTimpDeAsteptare(){
	int timpAsteptare = listaCozi.get(0).getWaitingTime();
	int idCoada = 0;
	for(Coada coada : listaCozi){
		if(coada.getWaitingTime()<timpAsteptare){
			timpAsteptare = coada.getWaitingTime();
			idCoada = listaCozi.indexOf(coada);
		}
	}
	return idCoada;
}
public int ceaMaiMicaCoada(){
	int index = 0;
	int numar = listaCozi.get(index).getSize();
	
	for(Coada coada : this.listaCozi){
		if(numar>coada.getSize()){
		numar = coada.getSize();
		index = this.listaCozi.indexOf(coada);
		}
	}
	System.out.println("coada cu cei mai putin clienti " + index + " nr clienti : "+numar);
	return index;
}
public BlockingQueue<Client> coadaTemporara(){
	
	return c1;	
}

public ArrayList<Coada> getCozi(){
	
	return this.listaCozi;
}
public int numarClientiLaCoada(int idCoada){
	return listaCozi.get(idCoada).getSize();
}
public ArrayList<Coada> getLista(){
	return this.listaCozi;
}
public static void setLog(String newLine){
	log.append(System.lineSeparator() +newLine);
	out.append(System.lineSeparator() +newLine);
}
public static void resetLog(){
	log.setLength(0);
}
public StringBuilder getLog(){
	return log;	
}
public Thread[] getThreaduri(){
	return threaduriCozi;
}
public  boolean getToateCozileSuntGoale(){
	return toateCozileSuntGoale;
}
public static void setPeek(String peek){
	peekHour = peek;
}
public static long getAverageWaitingTime(){
	timpTotalPentrecutLaCoada = CoadaConsumator.getTimpulTotalAteptare();
	return timpTotalPentrecutLaCoada / CoadaProducator.getTotalClientiProdusi();
}
public static long getAverageServiceTime(){
	long totalServiceTime = CoadaProducator.getTotalSeviceTime(); 
	return totalServiceTime / CoadaProducator.getTotalClientiProdusi();
}
public static void setTotalEmptyTime(long empty){
	totalEmptyTimeToateCozile = totalEmptyTimeToateCozile + empty;
}
public static long getTotalEmptyTime(){
	return totalEmptyTimeToateCozile;
}
}


