import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.sun.javafx.application.LauncherImpl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main {

	public class Maine extends Application{
		@Override
		public void start(Stage primaryStage) throws Exception{
		Parent root = FXMLLoader.load(getClass().getResource("GUITema2.fxml"));
		primaryStage.setTitle("AplicatiePolinoame");
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
		}
	}
	public static void main(String[] args) {
		Cozi cozi = new Cozi(4);
		BlockingQueue<Client> coada = new ArrayBlockingQueue<>(10);
		//PoolCozi pool = new PoolCozi(3);
		
		CoadaProducator producator= new CoadaProducator(cozi.coada1());
		//CoadaConsumator consumator = new CoadaConsumator(cozi.coada1());
		//Cozi cozi = new Cozi();
		new Thread(producator).start();
		
		new Thread(cozi).start();
		//new Thread(consumator).start();
		//new Thread(cozi).start();
		/*for (int i = 0;i<2;i++){
		new Thread(pool.getCoada(i)).start();
		}*/
		System.out.println("Threadurile au fost pornite");
		launch(args);
	}

}
